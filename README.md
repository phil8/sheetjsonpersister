# SheetJsonPersister Library     

## About

This is a library which provides persister to a Sequelize db conection for Json events representing worksheet rows

The library will create the DB schema and persist all events to the db connection.

The events are expected to conform this the following schema.
```
{
    table: 'Sheet Name',//A work sheet name
    schema: {
        'Column A': '[bigint],
        'Column B': '[real]
        'Column C': '[string]
    }
    rows: [{ //A row from the work sheet
        'Column A': 1,
        'Column B': 2.2,
        'Column C': 'test
    }]
}
```

## Install

```
npm install SheetJsonPersister
yarn install SheetJsonPersister
```

## How to use

```
import { SheetJsonPersister} from 'sheet-json-persister'
let event = { 
    table: 'Sheet1',
    schema: {
        'Column A': '[bigint],
        'Column B': '[real]
        'Column C': '[string]
    },
    rows: [{ 'Column A': 1, 'Column B': 1.1, 'Column C': 'test1', 'Column D': 1,'Column E': '1.1'  }]
}
let persister = SheetJsonPersister(sequelize)
await persister(event)
```

See the SheetJsonPersister unit test in the test directory for usage.