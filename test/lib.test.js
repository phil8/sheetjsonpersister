import { describe } from 'riteway'
import { isSQLKey, sqlTypeOf, asSQLName, SheetJsonPersister } from '../index.js'
const Sequelize = require('sequelize')
const debug = require('debug')('test')

const sequelize = new Sequelize(
  'projectcontrols',
  'sabr',
  'password', {
  host: 'localhost',
  dialect: 'sqlite',
  //dialect: 'mssql',
  logging: false // disable loggin
})

let row1 = { 'Column A': 1, 'Column B': 1.1, 'Column C': 'test1', 'Column D': 1, 'Column E': '1.1', 'Column F[primary]': 1, 'Column G [primaryKey,string]': 1 }
let row2 = { 'Column A': 2, 'Column B': 2.2, 'Column C': 'test2', 'Column D': 2, 'Column E': '2.2', 'Column F[primary]': 2, 'Column G [primaryKey,string]': 2 }

describe('SQLName', async (assert) => {
    assert({
      given: 'a sheet name',
      should: 'table name be valid sql name',
      actual: asSQLName('Sheet1'),
      expected: 'sheet1'
    })

    assert({
      given: 'a sheet name',
      should: 'table name be valid sql name',
      actual: asSQLName('Some Sheet1'),
      expected: 'some_sheet1'
    })

    assert({
      given: 'a sheet name',
      should: 'table name be valid sql name',
      actual: asSQLName('Some Sheet1 Name'),
      expected: 'some_sheet1_name'
    })

    assert({
      given: 'a sheet name',
      should: 'table name be valid sql name',
      actual: asSQLName('Some Sheet1 - Name'),
      expected: 'some_sheet1_name'
    })

    assert({
      given: 'a name with meta',
      should: 'return just the name',
      actual: asSQLName('Some Sheet1 [primaryKey]'),
      expected: 'some_sheet1'
    })

    assert({
      given: 'a name with &',
      should: 'return just the name',
      actual: asSQLName('Some&Sheet1'),
      expected: 'some_sheet1'
    })
    assert({
      given: 'a name with (',
      should: 'return just the name',
      actual: asSQLName('Some(Sheet1)'),
      expected: 'some_sheet1'
    })
    assert({
      given: 'a name with (',
      should: 'return just the name',
      actual: asSQLName('SomeSheet?'),
      expected: 'somesheet'
    })
})

describe('isSQLKey', async (assert) => {
  assert({
    given: 'a non key column name',
    should: 'be identified as a primary key',
    actual: isSQLKey('AColumn'),
    expected: false
  })
  assert({
    given: 'a key column name',
    should: 'be identified as a primary key',
    actual: isSQLKey('AColumn[primaryKey]'),
    expected: true
  })
})

describe('SqlTypeOf', async (assert) => {
  {
    let col = 'ColumnInt'
    let tableType = sqlTypeOf(col, 'bigint', {})
    assert({
      given: 'an int column as string',
      should: 'sql type of should be int',
      actual: tableType.rowTypes[col].typeDefinition.type.key,
      expected: Sequelize.BIGINT.key
    })
  }
  {
    let col = 'ColumnReal'
    let tableType = sqlTypeOf(col, 'real', {})
    assert({
      given: 'an real column as string',
      should: 'sql type of should be real',
      actual: tableType.rowTypes[col].typeDefinition.type.key,
      expected: Sequelize.REAL.key
    })
  }
  {
    let col = 'Column [date]'
    let tableType = sqlTypeOf(col, 'date', {})
    assert({
      given: 'a date column',
      should: 'sql type of should be date',
      actual: tableType.rowTypes[col].typeDefinition.type.key,
      expected: Sequelize.DATE.key
    })
  }
  {
    let col = 'Column F'
    let tableType = sqlTypeOf(col, 'primaryKey', {})
    assert({
      given: 'a name with primaryKey meta',
      should: 'return type as primary',
      actual: tableType.hasPrimary,
      expected: true
    })
  }
  {
    let col = 'Column G'
    let tableType = sqlTypeOf(col, 'primaryKey,string', {})
    assert({
      given: 'a name with string meta',
      should: 'return type as string',
      actual: tableType.rowTypes[col].typeDefinition.type.key,
      expected: Sequelize.STRING.key
    })
  }
})

describe('SheetJsonPersister', async (assert) => {
  let batch = {
    table: 'Sheet1',
    schema: {
      'ColumnA': '[primaryKey,string]',
      'ColumnB': '[bigint]'
    },
    rows: [{ 'ColumnA': 1, 'ColumnB': 1 }]
  }

  let persister = SheetJsonPersister(sequelize)

  await persister(batch)

  assert({
    given: 'An spreadsheet event',
    should: 'save to db',
    actual: '1',
    expected: '1'
  })
})

describe('SheetJsonPersister-Create', async (assert) => {
  let errorHappened = false

  let recordsCount = 10000
  let columnCount = 20

  let schema = {}
  for (var n = 0; n < columnCount; n++) {
    schema['col' + n] = 'string'
  }

  let rows = []
  for (var i = 0; i < recordsCount; i++) {
    let row = {}
    for (var j = 0; j < columnCount; j++) {
      row['col' + j] = `Some long text value ${i} ${j}`
    }
    rows.push(row)
  }

  let batch = {
    table: 'Sheet2',
    schema,
    rows
  }

  let persister = SheetJsonPersister(sequelize, { MAX_BULK: 20000, force: true, errorHandler: anerr => { errorHappened = true } })

  await persister(batch)

  await persister(batch)

  // await new Promise((resolve) => setTimeout(() => resolve(), 1000))

  assert({
    given: 'An spreadsheet event for primaryKey column',
    should: 'create is invoked',
    actual: errorHappened,
    expected: false
  })
})
