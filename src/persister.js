const debug = require('debug')('persister')

const Sequelize = require('sequelize')

function isSQLKey (meta) {
  return meta.indexOf('primaryKey') >= 0
}

function asSQLName (rowName) {
  let rawName = rowName
  if (rowName.indexOf('[') >= 0) {
    rawName = rowName.substring(0, rowName.indexOf('['))
  }
  rawName = rawName.replace(/[\\(\\)\\&?]+/g, ' ')
  return rawName.trim().replace(/[ -]+/g, '_').toLowerCase()
}

function sqlValue (value) {
  if (typeof value === 'number') { return value }
  return `'${value}'`
}

function sqlTypeOf (key, metaData, tableMeta) {
  // debug(`sqlTypeOf ${key}`)
  if (!tableMeta.rowTypes) {
    tableMeta.rowTypes = {}
  }

  let t = { type: Sequelize.STRING }

  let defaultValue = ''
  // debug(metaData.length)
  if (metaData.length > 0) {
    if (metaData.indexOf('real') >= 0) {
      t = { type: Sequelize.REAL }
      defaultValue = 0
    } else if (metaData.indexOf('bigint') >= 0) {
      t = { type: Sequelize.BIGINT }
      defaultValue = 0
    } else if (metaData.indexOf('date') >= 0) {
      t = { type: Sequelize.DATE }
      defaultValue = 0
    } else if (metaData.indexOf('text') >= 0) {
      t = { type: Sequelize.TEXT }
    } else if (metaData.indexOf('string') === -1) {
      // the default
    }
  }
  if (isSQLKey(metaData)) {
    t.primaryKey = true
  }

  let column = asSQLName(key)
  tableMeta.rowTypes[key] = {
    typeDefinition: t,
    defaultValue: defaultValue
  }
  if (t.primaryKey) {
    tableMeta.hasPrimary = true
  }
  // debug(tableMeta)
  return tableMeta
}

function SheetJsonPersister (sequelize, options = {}) {
  debug(`SheetJsonPersister`)
  let tableTypes = {}
  let { schema = 'dbo', sheet = null, errorHandler = null, force = false, MAX_BULK = 10000 } = options

  function updateType (meta, schema) {
    Object.keys(schema).forEach(key => {
      // debug(`update type ${key}`)
      sqlTypeOf(key, schema[key], meta)
    })
  }

  async function createTable (sequelize, tableName, schema) {
    let tableMeta = {}
    tableTypes[tableName] = tableMeta
    updateType(tableMeta, schema)
    // debug(tableMeta)
    let sequelRowTypeDef = Object.keys(tableMeta.rowTypes).reduce((h, n) => { h[asSQLName(n)] = tableMeta.rowTypes[n].typeDefinition; return h }, {})
    // debug(sequelRowTypeDef)
    let sequelizeType = sequelize.define(tableName, sequelRowTypeDef)
    await sequelizeType.sync({ force })
    debug(`Table Created [${tableName}]`)
    tableMeta.sequelizeType = sequelizeType
    return tableMeta
  }

  function createRecord (tableMeta, x) {
    let columns = ''
    let values = ''
    let instance = {}
    // debug(tableMeta)
    Object.keys(x).forEach((column, i, keys) => {
      let tidycolumn = column.trim()
      let colName = asSQLName(column)
      // debug(`[${column}] => [${colName}]`)
      if (tableMeta.rowTypes[tidycolumn] === undefined) {
        // debug(`${column} is not defined in the schema and will not be uploaded`)
        return
      }
      let t = tableMeta.rowTypes[tidycolumn].typeDefinition.type.key

      let thevalue = x[column]
      if (thevalue === null) {
        thevalue = tableMeta.rowTypes[column].defaultValue
      }
      if (t === 'REAL') {
        instance[colName] = parseFloat(thevalue.length > 0 ? thevalue : 0).toFixed(3)
      } else if (t === 'STRING') {
        instance[colName] = String(thevalue)
      } else if (t === 'TEXT') {
        instance[colName] = String(thevalue)
      } else {
        instance[colName] = thevalue
      }
    })
    return instance
  }

  async function process (batch) {
      let schema = batch.schema
      let tableName = asSQLName(batch.table)
      let meta = tableTypes[tableName]
      let bulk = []
      if (!meta) {
        debug(`Table to be created ${tableName}`)
        // firstRecord.tableName = tableName
        meta = await createTable(sequelize, tableName, schema)
        meta.tableName = tableName
        tableTypes[tableName] = meta
      }

      if (meta.hasPrimary) {
        debug(`Submitting upsert batch `)
        await meta.sequelizeType.destroy({ force: true, where: {} })

        let start = Date.now()
        await meta.sequelizeType.bulkCreate(bulk)
        let end = Date.now()
        debug(`Time taken ${(end - start) / 1000}`)

        return 'upsert'
    } else {
        debug(`Submitting create batch `)
        if (batch.replaceWhere) {
          if(!meta.replaceProcessed){
            await meta.sequelizeType.destroy({ force: true, where: batch.replaceWhere })
            meta.replaceProcessed=true
          }
        }
        for (var i = 0; i < batch.rows.length; i++) {
        let record = batch.rows[i]
        bulk.push(createRecord(meta, record))
        if (bulk.length > MAX_BULK) {
          debug(`Sending max bulk ${bulk.length} records`)
          let start = Date.now()
          await meta.sequelizeType.bulkCreate(bulk)
          let end = Date.now()
          debug(`Time taken ${(end - start) / 1000}`)

          bulk = []
        }
      }
      if (bulk.length > 0) {
        debug(`Sending ${bulk.length} records`)
        let start = Date.now()
        await meta.sequelizeType.bulkCreate(bulk)
        let end = Date.now()
        debug(`Time taken ${(end - start) / 1000}`)
      }
        return 'create'
    }
  }
  return process
}

module.exports = {
  isSQLKey,
  sqlTypeOf,
  asSQLName,
  SheetJsonPersister
}
